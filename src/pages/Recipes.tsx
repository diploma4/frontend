import React from 'react';
import { RecipeList, FilterMenu, AppBreadcrumb } from '../components';
import { Recipe } from '../entities/Recipe';
import { RouteComponentProps } from '@reach/router';
import { Layout } from 'antd';
import { Category, Cuisine, MenuType, eComplexity } from '../entities/enum';

const { Content, Sider } = Layout;

export const Recipes: React.FC<RouteComponentProps> = () => {
    debugger;
    let recipes: Recipe[] = [
        new Recipe({
            id: '1', 
            title: 'Борщ',
            author: 'Olenka Kalenka',
            image: "https://2recepta.com/recept/borshh/borshh.jpg",
            category: Category.Soups,
            cuisine: Cuisine.European,
            menuType: MenuType.Any,
            tags: ['Пасха', 'День буряка', 'Приворот на мужика'],
            numberOfServings: 6,
            cookingTime: 40,
            calories: 253,
            rating: 5,
            complexity: eComplexity.VeryDifficult,
            description: 'Борщ - первое блюдо Борщ - традиционное восточнославянское блюдо, которое готовится со свеклой, что придаёт ему характерный насыщенный цвет. В отличие от обычных супов он густой. Борщ считается национальным украинским первым блюдом, хотя он имеет место в кулинарном фонде России, Литвы, Польши, Румынии, Молдовы.'
        }),
        new Recipe({
            id: '2', 
            title: 'Блины',
            author: 'Vikenti Best',
            image: 'https://img09.rl0.ru/eda/c620x415i/s2.eda.ru/StaticContent/Photos/170210144850/170306091249/p_O.jpg',
            category: Category.Baking_Desserts,
            menuType: MenuType.Vegan,
            tags: ['Для детей'],
            numberOfServings: 6,
            calories: 344,
            rating: 3,
            complexity: eComplexity.Normal,
            description: 'Борщ - первое блюдо Борщ - традиционное восточнославянское блюдо, которое готовится со свеклой, что придаёт ему характерный насыщенный цвет. В отличие от обычных супов он густой. Борщ считается национальным украинским первым блюдом, хотя он имеет место в кулинарном фонде России, Литвы, Польши, Румынии, Молдовы.'
        }),
        new Recipe({
            id: '3', 
            title: 'Чебурек',
            author: 'Danilo Popy Brila',
            image: 'https://www.gastronom.ru/binfiles/images/00000043/00035082.jpg',
            category: Category.Any,
            cuisine: Cuisine.Oceanian,
            menuType: MenuType.Lean,
            calories: 256,
            rating: 4,
            complexity: eComplexity.VeryEasy,
            description: 'Чебуреки Чебурек - особенный пирожок, с хрустящей, но мягкой корочкой, истекающий ароматным соком, с сочной же мясной начинкой. Минимальный набор для теста на чебуреки — вода, соль и мука. ... И, если пробужденный обещанием чебуреков аппетит позволяет, тесто лучше отложить на несколько часов в холодильник.'
        }),
        new Recipe({
            id: '4', 
            title: 'Кекс',
            author: 'Danilo Popy Brila',
            image: 'https://way2day.com/wp-content/uploads/2017/12/Smetannyj-keks-min.jpg',
            category: Category.Any,
            cuisine: Cuisine.Oceanian,
            menuType: MenuType.Lean,
            calories: 256,
            rating: 4,
            complexity: eComplexity.Easy,
            description: 'Чебуреки Чебурек - особенный пирожок, с хрустящей, но мягкой корочкой, истекающий ароматным соком, с сочной же мясной начинкой. Минимальный набор для теста на чебуреки — вода, соль и мука. ... И, если пробужденный обещанием чебуреков аппетит позволяет, тесто лучше отложить на несколько часов в холодильник.'
        })
    ]

    return (
        <Layout>
            
            <Sider width={300} className="site-layout-background">
                <FilterMenu />
            </Sider>
            <Layout style={{ padding: '0 24px 24px' }}>
                <AppBreadcrumb/>
                <Content
                className="site-layout-background"
                style={{
                    padding: 0,
                    margin: 0,
                    minHeight: 280,
                }}
                >
                    <RecipeList recipes={recipes}/>
                </Content>
            </Layout>
        </Layout>
    )
}
