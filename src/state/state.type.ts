// import React from 'react';
// import { tCartListActions, tCartListState } from './cartList/types';
// import { tCheckoutActions, tCheckoutState } from './checkout/types';
// import { eSubscribersNotifyMoment } from '../../shared/SubscribersList';

// import { tNavigationActions, tNavigationState } from './navigation/types';
// import { IKoshykUi, tConfig } from '../app.type';

// export type tAppState = {
//   config: tConfig;
//   koshykUi: IKoshykUi;
//   cartList: tCartListState;
//   checkout: tCheckoutState;
//   navigation: tNavigationState;
// };

// export type tActions = tCartListActions | tCheckoutActions | tNavigationActions;

// export type tSubscribableEvents = tActions | { type: 'foobar'; payload: string };

// export type tExternalAction = tCartListActions;

// export type tAppStateReducers<A extends keyof any, R> = Partial<
//   Record<A, React.Reducer<tAppState, R>>
// >;

// export type tNotifySubscribersCallback = (
//   action: tSubscribableEvents,
//   when: eSubscribersNotifyMoment,
// ) => void;