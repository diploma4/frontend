export enum CartListActions {
    GO_NEXT = 'goNext',
    OPEN = 'open',
    FETCH_CARTS_LIST_SUCCESS = 'fetchCartsListSuccess',
    FETCH_CARTS_LIST_FAIL = 'fetchCartsListFail',
  }