import React from 'react';
import { Router } from '@reach/router';
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import { Recipes, Recipe } from './pages';
import { AppHeader } from './components/layout';

function App() {
  return (
    <Layout>
      <AppHeader />
      <Router>
        <Recipe path='/recipes/:id' />
        <Recipes default path='/recipes' />
      </Router>
    </Layout>
  );
}

export default App;
