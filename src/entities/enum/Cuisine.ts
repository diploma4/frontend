export enum Cuisine {
    Any = 'Будь-яка кухня',
    African = 'Африканська кухня',
    Asian = 'Азіатська кухня',
    European = 'Європейська кухня',
    Oceanian = 'Морська кухня'
}