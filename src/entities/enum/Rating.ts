export enum eRating {
    Terrible = 'Жахливий',
    Bad = 'Поганий',
    Normal = 'Нормальний',
    Good = 'Добре',
    Wonderful = 'Чудовий'
}
