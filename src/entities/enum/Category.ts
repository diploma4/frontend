export enum Category {
    Any = 'Будь-яка страва',
    Soups = 'Супи',
    Baking_Desserts = 'Випічка та десерти',
    Salads = 'Салати'
}