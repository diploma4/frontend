export enum eComplexity {
    VeryEasy = 'Рецепти для чайників',
    Easy = 'Легкі рецепти',
    Normal = 'Нелегко, несложно, приготовить можно',
    Difficult = 'Рецепти для професіоналів',
    VeryDifficult = 'Рецепти для майстрів кухоної справи'
}