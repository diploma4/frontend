import { Category, Cuisine, MenuType, eComplexity } from './enum';

export interface IRecipeParams {
    id: string;
    title: string;
    description?: string;
    image?: string;
    author?: string;

    category?: Category;
    cuisine?: Cuisine;
    menuType?: MenuType; 
    complexity?: eComplexity;
    cookingTime?: number;
    numberOfServings?: number;
    tags?: string[];

    rating?: number;
    like?: number;
    dislike?: number;

    calories: number;
}

export class Recipe {
    id: string;
    title: string;
    description?: string;
    image?: string;
    author?: string;

    category?: Category;
    cuisine?: Cuisine;
    menuType?: MenuType; 
    complexity?: eComplexity;
    cookingTime?: number;
    numberOfServings?: number;
    tags?: string[];

    rating?: number;
    like?: number;
    dislike?: number;
    calories: number;
    
    constructor(recipe: IRecipeParams) {
        this.id = recipe.id;
        this.title = recipe.title;
        this.description= recipe.description;
        this.image = recipe.image;
        this.author = recipe.author;

        this.category= recipe.category;
        this.cuisine= recipe.cuisine;
        this.menuType= recipe.menuType;
        this.complexity= recipe.complexity;
        this.cookingTime= recipe.cookingTime;
        this.numberOfServings= recipe.numberOfServings;
        this.tags = recipe.tags;

        this.rating = recipe.rating;
        this.like = recipe.like;
        this.dislike = recipe.dislike;
        this.calories = recipe.calories;
    }
}