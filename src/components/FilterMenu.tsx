import React, { useState } from 'react';
import { Select, Rate, Collapse, Slider } from 'antd';
import { Category, Cuisine, MenuType } from '../entities/enum';
import { Icon } from '@iconify/react';
import chefHat from '@iconify/icons-mdi/chef-hat';
import { ClockCircleOutlined } from '@ant-design/icons';

const { Option } = Select;
const { Panel } = Collapse;


const rating = ['Жахливий', 'Поганий', 'Нормальний', 'Добре', 'Чудовий'];
const ingredients = ['яблуко', 'ананас', 'банан', 'груша', 'лимон'];
const maxCaloric = 1000;
const minCaloric = 0;
const marks = {
  0: minCaloric,
  1000: maxCaloric,
};


export const FilterMenu: React.FC = () => {
  const [includedIngredients, setIncludedIngredients] = useState(['']);
  const [excludedIngredients, setExcludedIngredients] = useState(['']);

  function onIncludedIngredientsChange(selectedItems: string[]) {
    setIncludedIngredients(selectedItems);
  };

  function onExcludedIngredientsChange(selectedItems: string[]) {
    setExcludedIngredients(selectedItems);
  };

  const filteredIngredients = ingredients.filter(o => !includedIngredients.includes(o) && !excludedIngredients.includes(o));

  return (
    <>
      <Collapse defaultActiveKey={['1']}>
        <Panel showArrow={false} header="Основна інформація" key="1">

          <p>Категорія</p>
          <Select defaultValue={Category.Any} style={{ width: '100%' }}>
            {Object.keys(Category).map((key) => {
              return (
                <Option value={Category[key]} key={key}>
                  {Category[key]}
                </Option>
              );
            })}
          </Select>

          <p>Кухня</p>
          <Select defaultValue={Cuisine.Any} style={{ width: '100%' }}>
            {Object.keys(Cuisine).map((key) => {
              return (
                <Option value={Cuisine[key]} key={key}>
                  {Cuisine[key]}
                </Option>
              );
            })}
          </Select>

          <p>Тип меню</p>
          <Select defaultValue={MenuType.Any} style={{ width: '100%' }}>
            {Object.keys(MenuType).map((key) => {
              return (
                <Option value={MenuType[key]} key={key}>
                  {MenuType[key]}
                </Option>
              );
            })}
          </Select>

          <p>Складність страви</p>
          <Rate tooltips={rating} character={<Icon icon={chefHat} style={{ fontSize: 24 }}/>} />

          <p>Час приготування</p>
          <Rate tooltips={rating} character={<ClockCircleOutlined style={{ fontSize: 24 }}/>} />

        </Panel>
        <Panel header="Інгредієнти, деталі" key="2">

          <p>Включити інгредієнти</p>
          <Select 
            mode="multiple"
            placeholder="+ інгредієнт"
            onChange={onIncludedIngredientsChange}
            style={{ width: '100%' }} 
            tokenSeparators={[',']}
          >
            {
              filteredIngredients.map(item => (
                <Option key={item} value={item}>
                  {item}
                </Option>
              ))
            }
          </Select>
          
          <p>Виключити інгредієнти</p>
          <Select 
            mode="multiple"
            placeholder="- інгредієнт"
            onChange={onExcludedIngredientsChange}
            style={{ width: '100%' }} 
            tokenSeparators={[',']}
          >
            {
              filteredIngredients.map(item => (
                <Option key={item} value={item}>
                  {item}
                </Option>
              ))
            }
          </Select>

        </Panel>

        <Panel header="Поживна цінність" key="3">
          <p>Калорійність страви</p>
          <Slider 
            range 
            tooltipVisible
            marks={marks}
            max={maxCaloric} 
            min={minCaloric} 
            defaultValue={[300, 800]} />

          <p>Білки</p>
          <Slider 
            range 
            tooltipVisible
            marks={marks}
            max={maxCaloric} 
            min={minCaloric} 
            defaultValue={[300, 800]} />

          <p>Жири</p>
          <Slider 
            range 
            tooltipVisible
            marks={marks}
            max={maxCaloric} 
            min={minCaloric} 
            defaultValue={[300, 800]} />

          <p>Вуглеводи</p>
          <Slider 
            range 
            tooltipVisible
            marks={marks}
            max={maxCaloric} 
            min={minCaloric} 
            defaultValue={[300, 800]} />
        </Panel>
      </Collapse>
    </>
  );
};
