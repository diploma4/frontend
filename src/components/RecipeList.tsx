import React from 'react';
// import { Link } from '@reach/router'
import { Recipe } from '../entities/Recipe';
import { RecipePreview } from './RecipePreview';
import { List } from 'antd';

type tRecipeListProps = {
    recipes: Recipe[];
}

export const RecipeList: React.FC<tRecipeListProps> = ( props ) => {
    return (
        <>
            <List
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 1,
                    md: 1,
                    lg: 1,
                    xl: 1,
                    xxl: 4,
                }}
                dataSource={props.recipes}
                renderItem={item => (
                <List.Item>
                    {/* <Link to={`/recipes/${item.id}`}> */}
                        <RecipePreview recipe={item}/>
                    {/* </Link> */}
                </List.Item>
                )}
            />
        </>
    )
}