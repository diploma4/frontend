import React from 'react';
import { Layout, Menu } from 'antd';

const { Header } = Layout;

export const AppHeader: React.FC = () => {
    return (
        <Header className="header">
            <div className="logo" />
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                <Menu.Item key="1">Рецепти</Menu.Item>
                <Menu.Item key="2">Що мені захомячить?</Menu.Item>
            </Menu>
        </Header>
    )
}
