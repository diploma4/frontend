import React from 'react';
import { Card, Tag, Col, Row, Space, Rate, Typography, Button, Divider } from 'antd';
import { Recipe } from '../entities/Recipe';
import { Category, Cuisine, MenuType } from '../entities/enum';
import { ClockCircleOutlined, HeartOutlined } from '@ant-design/icons';
import { Icon } from '@iconify/react';
import foodtrayIcon from '@iconify/icons-whh/foodtray';
import fireIcon from '@iconify/icons-el/fire';
// import chefHat from '@iconify/icons-mdi/chef-hat';

const { Meta } = Card;
const { Paragraph } = Typography;

type tRecipePreviewProps = {
  recipe: Recipe;
};

const colors = [
  'magenta',
  'red',
  'volcano',
  'orange',
  'gold',
  'lime',
  'green',
  'cyan',
  'blue',
  'geekblue',
  'purple',
];

function getColor() {
  return colors[Math.floor(Math.random() * colors.length)];
}

export const RecipePreview: React.FC<tRecipePreviewProps> = (props) => {
  debugger;
  return (
    <>
      <Card
        hoverable
        cover={
          <img alt='example' style={{ width: '100%', height: '300px' }} src={props.recipe.image} />
        }
      >
        <Meta
          title={
            <>
              <Row style={{ margin: '0px', fontSize: '30px' }}>
                <Col>{props.recipe.title}</Col>
								<Col><Button shape='circle' icon={<HeartOutlined />} /></Col>
              </Row>
              <p>{props.recipe.rating && <Rate disabled defaultValue={props.recipe.rating} />}</p>
            </>
          }
        />
        <div>
          <Row>
            <Space size='middle'>
              <Col>
                <Icon icon={fireIcon} style={{ fontSize: '14px' }} /> {props.recipe.calories} ккал
              </Col>
              {props.recipe.numberOfServings && (
                <Col>
                  <Icon icon={foodtrayIcon} style={{ fontSize: '12px' }} />{' '}
                  {props.recipe.numberOfServings} порцій
                </Col>
              )}
              {props.recipe.cookingTime && (
                <Col>
                  <ClockCircleOutlined /> {props.recipe.cookingTime} хвилин
                </Col>
              )}
            </Space>
          </Row>

          <Paragraph
            ellipsis={{
              rows: 3,
              expandable: false,
            }}
            style={{ marginTop: '15px' }}
          >
            {props.recipe.description}
          </Paragraph>
        </div>
        <Divider />
        <div>
          {props.recipe.category && props.recipe.category != Category.Any && (
            <Tag color={getColor()}>{props.recipe.category}</Tag>
          )}
          {props.recipe.cuisine && props.recipe.cuisine != Cuisine.Any && (
            <Tag color={getColor()}>{props.recipe.cuisine}</Tag>
          )}
          {props.recipe.menuType && props.recipe.menuType != MenuType.Any && (
            <Tag color={getColor()}>{props.recipe.menuType}</Tag>
          )}
          {props.recipe.tags && props.recipe.tags.map((tag) => <Tag color={getColor()}>{tag}</Tag>)}
        </div>
      </Card>
    </>
  );
};
